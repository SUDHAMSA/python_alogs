def insertion_sort_asc(alist):
    for index in range(1, len(alist)):
        key = alist[index]
        i = index - 1
        while i >= 0 and alist[i] > key:
            alist[i + 1] = alist[i]
            i = i - 1
        alist[i + 1] = key
    return alist

def insertion_sort_desc(alist):
    for index in range(1, len(alist)):
        key = alist[index]
        i = index - 1
        while i >= 0 and alist[i] < key:
            alist[i + 1] = alist[i]
            i = i - 1
        alist[i + 1] = key
    return alist

