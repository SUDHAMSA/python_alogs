def is_palindrom(word):
    if not str(word).isalpha():
        return False
    else:
        if len(word) < 1:
            return False
        else:
            return word == word[::-1]
