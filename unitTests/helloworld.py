import unittest
from tdd import (
    helloworld,
    list_comphre
)
from simpleAlgos import (
    palindrom,
    prime
)


class MyFirstTests(unittest.TestCase):
    def test_hello(self):
        self.assertEqual(helloworld.hello_world(), 'hello world')

    def test_create_num_list(self):
        self.assertEqual(len(list_comphre.create_num_list(10)), 10)

    def test_is_palindrom_sad_path(self):
        self.assertEqual(palindrom.is_palindrom('hello'), False)

    def test_is_palidrom_empty(self):
        self.assertEqual(palindrom.is_palindrom(''), False)

    def test_is_palidrom_happy_path(self):
        self.assertEqual(palindrom.is_palindrom('cattac'), True)

    def test_is_palidrom_not_a_string(self):
        self.assertEqual(palindrom.is_palindrom(1), False)

    def test_is_prime_invalid_input(self):
        self.assertEqual(prime.is_prime(None), False)

    def test_is_prime_sad_path(self):
        self.assertEqual(prime.is_prime(10), False)

    def test_is_prime_happy_path(self):
        self.assertEqual(prime.is_prime(11), True)

    def test_is_prime_error_path(self):
        with self.assertRaises(TypeError):
            prime.is_prime("str")


if __name__ == '__main__':
    unittest.main()
