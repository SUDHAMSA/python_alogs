def is_prime(n):
    n = abs(int(n))

    if n < 2:
        return False

    if n == 2:
        return True

    if not n & 1:
        return False

    for x in range(3, int(n ** 0.5) + 1, 2):
        if n % x == 0:
            return False
    return True


number = input("Enter Number: ")

print is_prime(number)

# java script
# function isPrime(n){
#  if n < 2:
#     return false;
#  if n === 2:
#     return true;
#  if n%2 === 0:
#     return false;
#  const q = Math.floor(Math.sqrt(n));
#  for (var i = 3; i<=q; i = i+2){
#
#   }
#  return true;
# }
