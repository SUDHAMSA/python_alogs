import re

text_to_search = '''
abcdefghijklmnopqurtuvwxyz
ABCDEFGHIJKLMNOPQRSTUVWXYZ
1234567890
Ha HaHa
MetaCharacters (Need to be escaped):
. ^ $ * + ? { } [ ] \ | ( )
coreyms.com
321-555-4321
123.555.1234
123*555*1234
800-555-1234
900-555-1234
Mr. Schafer
Mr Smith
Ms Davis
Mrs. Robinson
Mr. T
_
'''

scentence = 'Start a sentence and then bring it to an end'

emails = '''
CoreyMSchafer@gmail.com
corey.schafer@university.edu
corey-321-schafer@my-work.net
'''

# pattern = re.compile(r'(Mr|Ms|Mrs)\.?\s[A-Z]\w*')
# print re.sub(r'_',' ','_hello_world')

pattern = re.compile(r'[a-zA-Z0-9.-]+@[a-zA-Z-]+\.(com|edu|net)')

matches = pattern.finditer(emails)

# with open('data.txt', 'r') as f:
#     contents = f.read()
#
#     pattern = re.compile(r'\d\d\d[-.]\d\d\d[-.]\d\d\d\d')
#     matches = pattern.finditer(contents)
#     for match in matches:
#         print match

for match in matches:
    print match
