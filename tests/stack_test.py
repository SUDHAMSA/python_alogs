from stacks import *

print balance_parenthesis.par_checker("()()())")
print balance_parenthesis.par_checker("(()")

print base_converter.base_converter(24, 8)
print base_converter.base_converter(24, 16)

print prefix_infix_postfix.infix_to_postfix("A + B * C / ( D - E )")
print prefix_infix_postfix.infix_to_postfix("( A + B ) * C")

print prefix_infix_postfix.postfix_evaluation("7 8 + 3 2 + /")
