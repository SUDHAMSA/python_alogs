from sorting.sorting import insertion_sort_asc, insertion_sort_desc

alist = [120, 34, 26, 45, 32]

print insertion_sort_asc(alist)
print insertion_sort_desc(alist)
