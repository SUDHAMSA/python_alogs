def sort(obj_list):
    """
    :param obj_list:
    :return:
    """
    obj_list.sort(key=lambda x: x['age'], reverse=True)
    return obj_list


sorted_list = sort([{'age': 31}, {'age': 21}, {'age': 100}])
print sorted_list
