def split_list2(alist):
    if len(alist) % 2 == 0:
        first_half = alist[:len(alist) / 2]
        second_half = alist[len(alist) / 2:]
    else:
        first_half = alist[:len(alist) / 2]
        second_half = alist[len(alist) / 2:-1]
        first_half.append(alist[-1])

    return first_half, second_half


print(split_list2([1, 2, 3, 4, 5]))
