def find_duplicates(word):
    """
    :param word:
    :return:
    """
    if len(word) <= 0:
        return None
    else:
        output = {}
        word_tuple = set(word)

        for key in word_tuple:
            key_count = word.count(key)
            if key_count > 1:
                output[key] = key_count
        return output


out = find_duplicates("Mississippi")
for i in out:
    print "{0} {1}".format(i, out[i])
