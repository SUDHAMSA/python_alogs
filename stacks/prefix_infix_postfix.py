from Stack import Stack


def infix_to_postfix(infix):
    """
    :param infix:
    :return:
    """

    prec = {}
    prec["**"] = 4
    prec["*"] = 3
    prec["/"] = 3
    prec["+"] = 2
    prec["-"] = 2
    prec["("] = 1

    op_stack = Stack()
    post_fix = []
    infix_list = infix.split(" ")
    for token in infix_list:
        if token in "ABCDEFGHIJKLMNOPQRSTUVWXYZ" or token in "0123456789":
            post_fix.append(token)
        elif token == "(":
            op_stack.push(token)
        elif token == ")":
            top_token = op_stack.pop()
            while top_token != "(":
                post_fix.append(top_token)
                top_token = op_stack.pop()
        else:
            while (not op_stack.isEmpty()) and (prec[op_stack.peek()] >= prec[token]):
                post_fix.append(op_stack.pop())
            op_stack.push(token)
    while not op_stack.isEmpty():
        post_fix.append(op_stack.pop())
    return " ".join(post_fix)


def postfix_evaluation(post_fix_expression):
    """

    :param post_fix_expression:
    :return:
    """
    operand_stack = Stack()
    expr_list = post_fix_expression.split(" ")
    for token in expr_list:
        if token in "0123456789":
            operand_stack.push(int(token))
        else:
            operand_2 = operand_stack.pop()
            operand_1 = operand_stack.pop()
            result = doMath(token, operand_1, operand_2)
            operand_stack.push(result)
    return operand_stack.pop()


def doMath(op, op1, op2):
    """
    :param op:
    :param op1:
    :param op2:
    :return:
    """
    if op == "*":
        return op1 * op2
    elif op == "/":
        return op1 / op2
    elif op == "+":
        return op1 + op2
    else:
        return op1 - op2
