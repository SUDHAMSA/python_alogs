from Stack import Stack


def base_converter(decimal_number, base):
    """

    :param decimal_number:
    :param base:
    :return:
    """
    digits = "0123456789ABCDEF"
    rem_stack = Stack()
    while decimal_number > 0:
        rem = decimal_number % base
        rem_stack.push(rem)
        decimal_number = decimal_number // base

    base_string = ""
    while not rem_stack.isEmpty():
        base_string = base_string + digits[rem_stack.pop()]

    return base_string
