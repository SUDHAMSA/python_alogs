from Stack import Stack


def par_checker(symbol_string):
    """
    :param symbol_string:
    :return:
    """
    balanced = True
    simple_stack = Stack()
    if len(symbol_string) % 2 == 0:
        index = 0
        while index < len(symbol_string):
            if symbol_string[index] == "(":
                simple_stack.push(symbol_string[index])
            else:
                if simple_stack.isEmpty():
                    balanced = False
                else:
                    simple_stack.pop()
            index = index + 1

    else:
        balanced = False
    if simple_stack.isEmpty() and balanced:
        return True
    else:
        return False
